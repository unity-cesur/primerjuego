using UnityEngine;

public class MenuController : MonoBehaviour
{
    public GameObject MenuPrincipal;
    public GameObject MenuOpciones;

    void Start()
    {
        // Al iniciar el juego, aseguramos que solo el menú principal esté activo
        ShowMenuPrincipal();
    }

    // Llama a este método para mostrar el Menu Principal y ocultar el Menu Opciones
    public void ShowMenuPrincipal()
    {
        MenuPrincipal.SetActive(true);
        MenuOpciones.SetActive(false);
    }

    // Llama a este método para mostrar el Menu Opciones y ocultar el Menu Principal
    public void ShowMenuOpciones()
    {
        MenuPrincipal.SetActive(false);
        MenuOpciones.SetActive(true);
    }
}
