using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GIFPlayer : MonoBehaviour
{
    public Image imageUI; // La referencia a la Image UI en el panel
    public string folderPath = "inicio"; // La carpeta dentro de Resources donde están los fotogramas
    public float frameRate = 0.05f; // La duración de cada fotograma en segundos

    private Sprite[] frames;
    private int currentFrame;
    private float timer;

    void Start()
    {
        LoadFrames();
        if (frames.Length > 0)
        {
            StartCoroutine(PlayGIF());
        }
    }

    void LoadFrames()
    {
        // Carga todos los sprites en la carpeta especificada
        frames = Resources.LoadAll<Sprite>(folderPath);
    }

    IEnumerator PlayGIF()
    {
        while (true)
        {
            timer += Time.deltaTime;

            if (timer >= frameRate)
            {
                timer = 0f;
                currentFrame = (currentFrame + 1) % frames.Length;
                imageUI.sprite = frames[currentFrame];
            }

            yield return null;
        }
    }
}
