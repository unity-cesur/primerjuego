using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class menu : MonoBehaviour
{
    public AudioMixer audiomixer;

    public void Salir(){
        Debug.Log("Te saliste");
        Application.Quit();
    }

    public void Juego(){
        SceneManager.LoadScene("Juego");
    }

    public void SetVolumen(float volume){
        audiomixer.SetFloat("Volumen",volume);
    }

    
}
