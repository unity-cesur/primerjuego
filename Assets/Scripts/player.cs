using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class player : MonoBehaviour
{
    float velocidadAvance = 10.0f;
    float velocidadRotacion = 150.0f;
    public playerAnimator py;
    public int vida = 100;

    //salto
    Rigidbody rb;
    int jumpCount = 0; // Contador de saltos
    public int maxJumpCount = 2; // Número máximo de saltos
    bool isJump = false;
    bool suelo=false;
    public float jumpforce = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //moviemiento
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        float avance = vertical * velocidadAvance * Time.deltaTime;
        float rotacion = horizontal * velocidadRotacion * Time.deltaTime;

        transform.Rotate(Vector3.up, rotacion, Space.Self);
        transform.position += transform.forward * avance;
        //animacion
        if (horizontal != 0 || vertical != 0)
        {
            py.speed = 1;
        }
        else
        {
            py.speed = 0;
        }
        //salto

        isJump = Input.GetButtonDown("Jump");

        if (Input.GetButtonDown("Jump") && jumpCount < maxJumpCount)
        {
            rb.AddForce(new Vector3(0, jumpforce, 0), ForceMode.Impulse);
            jumpCount++;
            suelo = false;  // Se establece en falso después de saltar
        }

    }
    //daño
    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Muerte")){
            Debug.Log("Mueres");
        }else if(collision.gameObject.CompareTag("Danio")){
            Debug.Log("Recibes daño");
            vida -= 10;
            if(vida <= 0){
                SceneManager.LoadScene("Lose");
            }
        }else if (collision.gameObject.CompareTag("suelo"))  // Verifica si el personaje toca el suelo
        {
            suelo = true;
            jumpCount = 0;
        }
        
    }
    
    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("suelo"))  // Verifica si el personaje deja de tocar el suelo
        {
            suelo = false;
        }
    }
//llave
    public bool key = false;
    void OnTriggerEnter(Collider collider){
        if(collider.CompareTag("llave")){
            Destroy(collider.gameObject); // Destruye el objeto que contiene el collider
            AddKey();
        } else if(collider.CompareTag("puerta") && key){
            Destroy(collider.gameObject); // Destruye el objeto que contiene el collider
        }
    }
    
    public void AddKey(){
        Debug.Log("Llave conseguida");
        key=true;
    }
}
